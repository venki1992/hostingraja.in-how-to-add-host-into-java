The following steps are to add host into java at HostingRaja server

Script:

if [ "$#" -ne 3 ]; then

  echo "Enter All Three Arguments" >&2
  
  exit 1
  
fi

DOMAIN_NAME=$1

FILE_FULL_PATH=$2

TOMCAT_PATH=$3

CONF_PATH="conf/server.xml"


GREP_PATH="$TOMCAT_PATH$CONF_PATH"

MANAGER_PATH="webapps/manager"

DOCBASE="$TOMCAT_PATH$MANAGER_PATH"

GREPLINE="<Alias>www.$DOMAIN_NAME</Alias>"

if grep -Fxq "$GREPLINE" $GREP_PATH

then

    # code if found
    
echo "Domain is already added"

else

    # code if not found
    
GREP_LIB=`whereis grep | awk '{print $2}'`

Line_NO=`$GREP_LIB -ni 'Engine' $GREP_PATH | tail -1 | awk -F '[/:]' '{print $1}'`

Line_NO=$((Line_NO-1))

#echo $Line_NO

ADDED_LINE="<Host name=\"$DOMAIN_NAME\"  appBase=\"$FILE_FULL_PATH\" unpackWARs=\"false\" deployXML=\"false\">\n<Alias>www.$DOMAIN_NAME</Alias>\n<Context path=\"\" reloadable=\"true\" docBase=\"$FILE_FULL_PATH\" debug=\"1\"/>\n<Context path=\"/manager\" debug=\"0\" privileged=\"true\" docBase=\"$DOCBASE\">\n</Context>\n</Host>";

EXEC_COMMAND="sed -i '$Line_NO a  $ADDED_LINE' $GREP_PATH"

BASH_LIB=`whereis bash | awk '{print $2}'`

#eval $EXEC_COMMAND

$BASH_LIB -c "$EXEC_COMMAND"

SERVICE_LIB=`whereis service | awk '{print $2}'`

SERVICE_RESTART="$SERVICE_LIB tomcat restart"

#eval $SERVICE_RESTART

$SERVICE_RESTART

fi

Explanation:

In the above script, we will need to pass three arguments.  The first argument is DOMAIN_NAME example we will need to give ( hosigraja.in )

the second argument as FILE_FULL_PATH ("/home/hostingraja/public_html/") and third argument as 

TOMCAT_PATH example /usr/local/tomcat8/ 


The above script helpful to avoid the duplication of adding domain into a server.xml file in tomcat.  


This configuration avoids the WAR file deployment so easily java site makes live. if you have made the changes in the particular file then no need to restart the server that configuration reflects the changes immediately. 

In our HostingRaja, we have supported the Tomcat 7 , 8 and 9 too .

HostingRaja provides the VPS servers at very affordable prices. Navigate to hostingraja.in to know more.
